===============
windmill-backup
===============

An Ansible deployment to validate ansible roles for Continuous Integration

* License: Apache License, Version 2.0
* Documentation: https://windmill-backup.readthedocs.org
* Source: https://opendev.org/windmill/windmill-backup
* Bugs: https://bugs.launchpad.net/windmill-backup

Description
-----------

Windmill is a collection of Ansible playbooks and roles used to deploy
Continuous Integration (CI) tools. It serves to be a functional test environment
for our Ansible CI roles primarily, however it also provides an example
deployment for users.
