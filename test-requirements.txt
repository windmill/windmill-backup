hacking<0.11,>=0.10
ansible-lint
ara<1.0.0
bashate
sphinx>=1.1.2,!=1.2.0,!=1.3b1,<1.3
yamllint
